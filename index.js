var startButton,
	allCells,
	message;

var xMove = "x";
var oMove = "o";
var currentMove;
var totalMoves = 0;
var minWinMoves = 5;
var winner;

window.addEventListener('load', initJS);

function initJS(event) {
	startButton = document.querySelector('.startNewGame');
	allCells = document.querySelectorAll('.cell');
	message = document.querySelector('.winner-message');

	startButton.addEventListener('click', startButton_click);
}

function startButton_click(event) {
	console.log("CLICKSTART");
	totalMoves = 0;
	currentMove = xMove;
	message.innerText = "";
	for (var i=0; i<allCells.length; i++) {
		allCells[i].classList.remove(xMove);
		allCells[i].classList.remove(oMove);
		allCells[i].addEventListener('click', cells_click);
	}
}

function cells_click(event) {
	var cellClassList = event.currentTarget.classList;
	if (cellClassList.contains(xMove) || cellClassList.contains(oMove)) {
		return;
	}

	cellClassList.add(currentMove);

	totalMoves++;
	if (totalMoves >= minWinMoves && getWinner()) {
		winner = getWinner();
		finish();
		return;
	}

	if (totalMoves == allCells.length) {
		winner = "friendship";
		finish();
	}

	currentMove = (currentMove == xMove) ? oMove : xMove;
}

function finish() {
	for (var i=0; i<allCells.length; i++) {
		allCells[i].removeEventListener('click', cells_click);
	}
	message.innerText = "The winner is: " + winner.toUpperCase();
}